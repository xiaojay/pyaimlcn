#coding=utf-8
import aiml
import os
import config as cfg

def is_cn_char(uchar): 
     if uchar >= u'\u4e00' and uchar<=u'\u9fa5':
         return True
     return False

def is_en_char(uchar):
    if (uchar >= u'\u0041' and uchar<=u'\u005a') or (uchar >= u'\u0061' and uchar<=u'\u007a'):
        return True
    return False

def is_number(uchar):
    if uchar >= u'\u0030' and uchar<=u'\u0039':
        return True
    return False

def tell_type(uchar):
    if uchar >= u'\u4e00' and uchar<=u'\u9fa5':
        return 'cn'
    if (uchar >= u'\u0041' and uchar<=u'\u005a') or (uchar >= u'\u0061' and uchar<=u'\u007a'):
        return 'en'
    if uchar >= u'\u0030' and uchar<=u'\u0039':
        return 'number'
    return 'other'

def add_space(sentence):
    r = []
    pre_type = ''
    for c in sentence:
        type_ = tell_type(c)
        if pre_type and type_ != pre_type or type_ == 'cn':
            r.append(' ')
        r.append(c)
        pre_type = type_

    if r[0] == ' ':
        r = r[1:]
    
    return ''.join(r)

def q2b(uchar):
    """全角转半角"""
    inside_code=ord(uchar)
    if inside_code==0x3000:
        inside_code=0x0020
    else:
        inside_code-=0xfee0
    if inside_code<0x0020 or inside_code>0x7e:      #转完之后不是半角字符返回原来的字符
        return uchar
    return unichr(inside_code)

def q2b2(sentence):
    r = ''.join( [ q2b(w) for w in sentence])
    return r

class TaoBot(aiml.Kernel):
    def __init__(self, aiml_fn='', properties=cfg.BOT_PROPERTIES):
        aiml.Kernel.__init__(self)
        self.verbose(cfg.DEBUG)
        
        if not aiml_fn:
            if os.path.isfile(cfg.BRAIN):
                self.bootstrap(brainFile = cfg.BRAIN)
            else:
                self.init_bot()
                self.saveBrain(cfg.BRAIN)
        else:
            self.learn(aiml_fn)

        for p in properties:
            self.setBotPredicate(p, properties[p] )
    
    def init_bot(self):
        for fn in os.listdir(cfg.AIML_SET):
            if os.path.splitext(fn)[-1] == '.aiml':
                self.learn(os.path.join(cfg.AIML_SET, fn))
    
    def respond(self, words):
        if type(words) == type('中'):
            words = unicode(words, 'u8')
        words = add_space( q2b2(words) )
        return aiml.Kernel.respond(self, words)
